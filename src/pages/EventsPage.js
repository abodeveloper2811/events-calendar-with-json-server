import React, {useEffect, useState} from 'react';
import PostAddIcon from '@mui/icons-material/PostAdd';
import {useDispatch, useSelector} from "react-redux";
import {getEvents, setUpdateEventId} from "../redux/actions/eventsActions";
import AddEvent from "../components/Events-CRUD/AddEvent";
import EditEvent from "../components/Events-CRUD/EditEvent";
import DeleteEvent from "../components/Events-CRUD/DeleteEvent";
import moment from 'moment';
import {Calendar, dateFnsLocalizer} from 'react-big-calendar'
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import startOfWeek from 'date-fns/startOfWeek';
import getDay from 'date-fns/getDay';
import getMonth from 'date-fns/getMonth';
import enUS from 'date-fns/locale/en-US';
import "./events_page.scss";

const locales = {
    'en-US': enUS,
};

const localizer = dateFnsLocalizer({
    format,
    parse,
    startOfWeek,
    getDay,
    getMonth,
    locales,
});


const EventsPage = () => {

    const [show, setShow] = useState(false);
    const [eventList, setEventList] = useState([]);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const toggle = () => setShow(!show);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getEvents());
    }, []);

    const {events, loading} = useSelector(state => state.events);

    const openEventClick = (event) => {
        dispatch(setUpdateEventId(event.id));
    };

    useEffect(() => {

        const leaves = [];
        events.forEach((item) => {
            let start_at = (new Date(item.start_date));
            let end_at = (new Date(item.end_date));
            leaves.push({
                id: item.id,
                title: `${item.title} - ${item.description}`,
                start: start_at,
                end: end_at,
                color: "#198754",
                type: 'leave',
                allDay: 'false'
            })
        });

        const list = [...leaves];

        setEventList(eventList => list);

        console.log(eventList);

    }, [events]);

    return (
        <>
            <div className="Events">

                <div className="container">

                    <div className="content">
                        <div className="row mb-4">
                            <div className="col-md-12">
                                <div className="d-flex align-items-center justify-content-between text-box">
                                    <h2 className="text-success fw-bold">Events Calendar</h2>

                                    <button onClick={handleShow} className="btn btn-success">
                                        <span className="me-2">Add Event</span> <PostAddIcon/>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div className="calendar-box">


                            <Calendar
                                /*selectable
                                step={3}
                                timeslots={10}*/
                                localizer={localizer}
                                events={eventList}
                                startAccessor="start"
                                endAccessor="end"
                                defaultDate={moment().toDate()}
                                style={{height: "85vh"}}
                                eventPropGetter={event => {
                                    const eventData = eventList.find(ot => ot.id === event.id);
                                    const backgroundColor = eventData && eventData.color;
                                    return {style: {backgroundColor, borderRadius: "2px", fontWeight: "bold"}};
                                }}
                                onSelectEvent={openEventClick}
                            />


                        </div>
                    </div>

                </div>

            </div>

            <AddEvent toggle={toggle} show={show} handleClose={handleClose}/>

            <EditEvent/>

            <DeleteEvent/>


        </>
    );
};

export default EventsPage;