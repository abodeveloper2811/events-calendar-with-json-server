import {
    BTN_LOADING_FALSE,
    BTN_LOADING_TRUE,
    GET_EVENTS,
    LOADING_FALSE,
    LOADING_TRUE,
    REMOVE_DELETE_EVENT,
    REMOVE_UPDATE_EVENT,
    SET_DELETE_EVENT_ID,
    SET_UPDATE_EVENT_ID
} from "../actionTypes/actionTypes";

const initialState = {

    events: [],

    delete_event: null,
    update_event: null,

    loading: false,
    btn_loading: false,
};


export const eventsReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type) {

        case LOADING_TRUE:

            return {
                ...state,
                loading: true
            };

        case BTN_LOADING_TRUE:

            return {
                ...state,
                btn_loading: true
            };

        case LOADING_FALSE:

            return {
                ...state,
                loading: false
            };

        case BTN_LOADING_FALSE:

            return {
                ...state,
                btn_loading: false
            };

        case GET_EVENTS:

            return {
                ...state,
                events: payload,
                loading: false
            };

        case SET_DELETE_EVENT_ID:

            return {
                ...state,
                delete_event: state.events.find((item) => item.id === payload.eventID)
            };

        case SET_UPDATE_EVENT_ID:

            return {
                ...state,
                update_event: state.events.find((item) => item.id === payload.eventID)
            };

        case REMOVE_DELETE_EVENT:

            return {
                ...state,
                delete_event: null
            };

        case REMOVE_UPDATE_EVENT:

            return {
                ...state,
                update_event: null
            };

        default:
            return state;

    }


};
