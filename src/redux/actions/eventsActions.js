import {
    BTN_LOADING_FALSE,
    BTN_LOADING_TRUE,
    GET_EVENTS,
    LOADING_FALSE,
    LOADING_TRUE, REMOVE_UPDATE_EVENT,
    SET_DELETE_EVENT_ID,
    SET_UPDATE_EVENT_ID
} from "../actionTypes/actionTypes";
import {PATH_NAME} from "../../tools/constant";
import axios from "axios";
import {toast} from "react-toastify";

export const getEvents = () => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`${PATH_NAME}/events/`);

        dispatch({
            type: GET_EVENTS,
            payload: res.data,
        });

    } catch (err) {
        dispatch({
            type: LOADING_FALSE,
        });
    } finally {
        dispatch({
            type: LOADING_FALSE,
        });
    }

};

export const addEvent = (data, handleClose) => async (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    try {

        const res = await axios.post(`${PATH_NAME}/events/`, data);

        handleClose();

        toast.success(`Event created successfully !!!`);

        dispatch(getEvents());

    } catch (err) {

        dispatch({
            type: BTN_LOADING_FALSE,
        });

        toast.error(`Event not created !!!`);
    } finally {
        dispatch({
            type: BTN_LOADING_FALSE,
        });
    }

};

export const setDeleteEventId = (eventID) => async (dispatch) => {

    try {

        dispatch({
            type: SET_DELETE_EVENT_ID,
            payload: {
                eventID
            }
        });

    } catch (err) {
        console.log(err)
    }
};

export const deleteEvent = (eventID, handleClose) => async (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    try {

        const res = await axios.delete(`${PATH_NAME}/events/${eventID}/`);

        dispatch({
            type: REMOVE_UPDATE_EVENT
        });

        handleClose();

        toast.success(`Event deleted successfully !!!`);

        dispatch(getEvents());

    } catch (err) {

        dispatch({
            type: BTN_LOADING_FALSE,
        });

        toast.error(`The event was not deleted !!!`);
    } finally {
        dispatch({
            type: BTN_LOADING_FALSE,
        });
    }

};

export const setUpdateEventId = (eventID) => async (dispatch) => {

    try {

        dispatch({
            type: SET_UPDATE_EVENT_ID,
            payload: {
                eventID
            }
        });

    } catch (err) {
        console.log(err)
    }
};

export const updateEvent = (eventID, data, handleClose) => async (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    try {

        const res = await axios.put(`${PATH_NAME}/events/${eventID}/`, data);

        handleClose();

        toast.success(`Event information updated successfully !!!`);

        dispatch(getEvents());

    } catch (err) {

        dispatch({
            type: BTN_LOADING_FALSE,
        });

        toast.error(`Event information not updated !!!`);
    } finally {
        dispatch({
            type: BTN_LOADING_FALSE,
        });
    }

};

