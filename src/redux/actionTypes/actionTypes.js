export const LOADING_TRUE = "LOADING_TRUE";
export const LOADING_FALSE = "LOADING_FALSE";

export const BTN_LOADING_FALSE = "BTN_LOADING_FALSE";
export const BTN_LOADING_TRUE = "BTN_LOADING_TRUE";
export const GET_EVENTS = "GET_EVENTS";
export const REMOVE_DELETE_EVENT = "REMOVE_DELETE_EVENT";
export const REMOVE_UPDATE_EVENT = "REMOVE_UPDATE_EVENT";
export const SET_DELETE_EVENT_ID = "SET_DELETE_EVENT_ID";
export const SET_UPDATE_EVENT_ID = "SET_UPDATE_EVENT_ID";





