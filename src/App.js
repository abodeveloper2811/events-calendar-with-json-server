import './App.css';
import {useEffect, useState} from "react";
import SiteLoader from "./components/SiteLoader/SiteLoader";
import {ToastContainer} from "react-toastify";
import EventsPage from "./pages/EventsPage";

//json-server --watch db.json --port 3002

function App() {

    const [site_loading, setSiteLoading] = useState(true);

    useEffect(() => {

        setTimeout(()=>{
            setSiteLoading(false);
        },1000);

    }, []);

    if (site_loading) {
        return (
            <SiteLoader/>
        )
    } else{

        return (
            <>
                <div className="App">
                    <EventsPage/>
                </div>

                <ToastContainer autoClose={2000}/>
            </>
        );
    }

}

export default App;
