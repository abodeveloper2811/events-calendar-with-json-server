import React, {Fragment, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Modal, ModalBody, ModalHeader} from 'reactstrap';
import {addEvent} from "../../redux/actions/eventsActions";
import SaveAsIcon from '@mui/icons-material/SaveAs';

const AddEvent = ({handleClose, show, toggle}) => {

    const dispatch = useDispatch();

    const {btn_loading} = useSelector(state => state.events);

    const handleSubmit = (event, data) => {
        dispatch(addEvent(data, handleClose));
    };

    return (
        <Fragment>

            <Modal isOpen={show} toggle={toggle}>

                <ModalHeader className="bg-success text-white" toggle={toggle}><b>Add Event</b></ModalHeader>

                <ModalBody>

                    <AvForm className="w-100" onValidSubmit={handleSubmit}>

                        <AvField
                            type="text"
                            label="Event Title"
                            name="title"
                            className="form-control"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Please enter an event title !'}
                            }}
                        />

                        <div className="mb-3"/>

                        <AvField
                            type="textarea"
                            label="Event Description"
                            name="description"
                            className="form-control"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Please enter an event description !!'}
                            }}
                        />

                        <div className="mb-3"/>

                        <AvField
                            type="date"
                            label="Start Date"
                            name="start_date"
                            className="form-control"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Please enter a start date !'}
                            }}
                        />

                        <div className="mb-3"/>

                        <AvField
                            type="date"
                            label="End Date"
                            name="end_date"
                            className="form-control"
                            validate={{
                                required: {value: true, errorMessage: 'Please enter a end date !'}
                            }}
                        />

                        <div className="mb-3"/>

                        <div className="d-flex mt-4">
                            <button id="quit-btn" onClick={handleClose} type="button"
                                    className="w-50 border border-2 border-success text-success btn fw-bold">CANCEL
                            </button>
                            <button id="submit-btn" type="submit" className="w-50 fw-bold ms-2 text-white bg-success btn"
                                    disabled={btn_loading}>SAVE <SaveAsIcon/>
                            </button>
                        </div>

                    </AvForm>

                </ModalBody>
            </Modal>

        </Fragment>
    );
};

export default AddEvent;