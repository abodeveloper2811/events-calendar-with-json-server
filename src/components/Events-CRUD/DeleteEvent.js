import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {REMOVE_DELETE_EVENT} from "../../redux/actionTypes/actionTypes";
import {deleteEvent} from "../../redux/actions/eventsActions";

const DeleteEvent = () => {

    const dispatch = useDispatch();

    const {delete_event, btn_loading} = useSelector(state => state.events);

    const closeModal = () =>{
        dispatch({
            type: REMOVE_DELETE_EVENT
        })
    };

    const onConfirm = () =>{
        dispatch(deleteEvent(delete_event?.id, closeModal))
    };

    let condition = false;
    if(delete_event !== null)
        condition = true;

    return (
        <div>

            <Modal isOpen={condition} toggle={closeModal}>
                <ModalHeader className="bg-danger text-white" toggle={closeModal}><b>Delete Event</b></ModalHeader>
                <ModalBody>
                    Are you sure you want to delete this event named <b>{delete_event?.title}</b> ?
                </ModalBody>

                <ModalFooter>
                    <button id="quit-btn" onClick={closeModal} className="border border-2 border-success text-success btn fw-bold">NO</button>
                    <button id="submit-btn" disabled={btn_loading} onClick={onConfirm} className="fw-bold btn btn-danger text-white">YES</button>
                </ModalFooter>
            </Modal>
        </div>
    );
};

export default DeleteEvent;