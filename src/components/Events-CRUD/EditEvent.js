import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {REMOVE_UPDATE_EVENT} from "../../redux/actionTypes/actionTypes";
import {setDeleteEventId, updateEvent} from "../../redux/actions/eventsActions";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import SaveAsIcon from '@mui/icons-material/SaveAs';

const EditEvent = () => {

    const dispatch = useDispatch();

    const {update_event, btn_loading} = useSelector(state => state.events);

    const closeModal = () => {
        dispatch({
            type: REMOVE_UPDATE_EVENT
        })
    };

    const setDeleteEvent = (id) => {
        dispatch(setDeleteEventId(id));
    };

    const onSubmit = (event, data) => {
        dispatch(updateEvent(update_event?.id, data, closeModal));
    };

    let condition = false;
    if (update_event !== null)
        condition = true;

    return (
        <div>

            <Modal isOpen={condition} toggle={closeModal}>
                <ModalHeader className="text-success fw-bold" toggle={closeModal}><b>Update Event</b></ModalHeader>
                <ModalBody>

                    <AvForm className="w-100" model={update_event} onValidSubmit={onSubmit}>

                        <AvField
                            type="text"
                            label="Event Title"
                            name="title"
                            className="form-control"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Please enter an event title !'}
                            }}
                        />

                        <div className="mb-3"/>

                        <AvField
                            type="textarea"
                            label="Event Description"
                            name="description"
                            className="form-control"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Please enter an event description !!'}
                            }}
                        />

                        <div className="mb-3"/>

                        <AvField
                            type="date"
                            label="Start Date"
                            name="start_date"
                            className="form-control"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Please enter a start date !'}
                            }}
                        />

                        <div className="mb-3"/>

                        <AvField
                            type="date"
                            label="End Date"
                            name="end_date"
                            className="form-control"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Please enter a end date !'}
                            }}
                        />

                        <div className="mb-3"/>

                        <div className="d-flex mt-4">
                            <button id="quit-btn" onClick={()=>setDeleteEvent(update_event?.id)} type="button"
                                    className="w-100 text-white btn btn-danger fw-bold">DELETE <DeleteForeverIcon/>
                            </button>
                        </div>

                        <div className="d-flex mt-2">
                            <button id="quit-btn" onClick={closeModal} type="button"
                                    className="w-50 border border-2 border-success text-success btn fw-bold">CANCEL
                            </button>
                            <button id="submit-btn" type="submit" className="w-50 ms-2 fw-bold text-white btn-success btn"
                                    disabled={btn_loading}>SAVE <SaveAsIcon/>
                            </button>
                        </div>

                    </AvForm>
                </ModalBody>
            </Modal>

        </div>
    );
};

export default EditEvent;