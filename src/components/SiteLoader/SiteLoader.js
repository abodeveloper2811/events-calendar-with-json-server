import React from 'react';
import { SemipolarLoading } from 'react-loadingg';

function SiteLoader() {
    return (
        <div className="SiteLoader">
            <SemipolarLoading size={"large"} color={"#198754"}/>
        </div>
    );
}

export default SiteLoader;